import paramiko
import socket
from struct import*
import time

modem_ip = '192.16.1.6'#'192.16.1.101'
modem_user = "root"
modem_password = "123456"
stop = 0

def command_and_repliy(ssh,command):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    output = ssh_stdout.read()
    return output


def create_socket(sockip,sockport):
    try:
        sockname = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockname.bind((sockip, sockport))
    except socket.error, msg:
        print "server :"
        print 'Socket could not be created.', str(msg[0]) + 'Message' + msg[1]
        sys.exit()
    return sockname


def ssh_connection():

    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    rpi = {"username": modem_user, "password": modem_password, "hostname": modem_ip}
    try:
        ssh.connect(**rpi)
        print "connected"

    except:
        print "connection failed try again"
    return ssh



sock = create_socket('192.168.70.91',12345)
sock.settimeout(5)
while stop != 1:
    try:
        sock.recvfrom(100)
        stop =1
    except:
        print "no packet yet"

